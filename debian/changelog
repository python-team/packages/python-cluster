python-cluster (1.4.1.post3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/: Apply "wrap-and-sort -abst".
  * debian/control: Bump Standards-Version to 4.6.1.

 -- Boyuan Yang <byang@debian.org>  Sat, 22 Oct 2022 12:10:36 -0400

python-cluster (1.3.3-4) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Bump debhelper from old 12 to 13.

  [ Boyuan Yang ]
  * Update standards version to 4.6.1, no changes needed.

  [ Luciano Bello ]
  * removing Luciano as uploader, as he is retiring.

 -- Boyuan Yang <byang@debian.org>  Sat, 21 May 2022 12:49:59 -0400

python-cluster (1.3.3-3) unstable; urgency=medium

  * Team upload.
  * d/copyright: Change upstream license from GPL to LGPL.

 -- Ondřej Nový <onovy@debian.org>  Mon, 12 Aug 2019 22:39:40 +0200

python-cluster (1.3.3-2) unstable; urgency=medium

  * Team upload.
  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).

  [ Adrian Alves ]
  * add python3 support

  [ Luciano Bello ]
  * Testsuite: autopkgtest-pkg-python added. Basic import tests.

 -- Ondřej Nový <onovy@debian.org>  Sat, 10 Aug 2019 23:40:26 +0200

python-cluster (1.3.3-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add the missing dependency on python-pkg-resources.
    (Closes: #896298)

 -- Adrian Bunk <bunk@debian.org>  Sun, 10 Jun 2018 14:01:42 +0300

python-cluster (1.3.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Luciano Bello ]
  * Watch file fixed.
  * Homepage updated.
  * New Build-Depend added: python-setuptools.

  [ Adrian Alves ]
  * New upstream release 1.3.3
  * fix standards-version 3.9.7

 -- Adrian Alves <aalves@gmail.com>  Sun, 24 Apr 2016 12:54:32 +0200

python-cluster (1.1.1b3-2) unstable; urgency=low

  * Team upload.

  [ Jan Dittberner ]
  * add debian/watch; Closes: #520102

  [ Jakub Wilk ]
  * Add Vcs-* fields.

  [ SVN-Git Migration ]
  * git-dpm config.
  * Update Vcs fields for git migration.

  [ Mattia Rizzolo ]
  * debian/source/format: set to 3.0 (quilt).
  * Move to debhelper compat 9 and dh short format.
  * Port to dh-python2 from python-support.  Closes: #785999
  * debian/control: bump Standards-Version to 3.9.6, no changes needed.
  * Fix lintian warning spelling-error-in-description.

 -- Mattia Rizzolo <mattia@debian.org>  Sat, 12 Dec 2015 19:10:27 +0000

python-cluster (1.1.1b3-1) unstable; urgency=low

  * Initial release (Closes: #483458)

 -- Luciano Bello <luciano@debian.org>  Thu, 29 May 2008 19:26:36 -0300
